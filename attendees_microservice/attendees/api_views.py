from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Attendee, ConferenceVO
from common.json import ModelEncoder


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeesListEncoder(ModelEncoder):
    """A class to encode a list of the attendees."""

    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    """A class to encode the details of an Attendee."""

    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    'GET' - returns a list of attendees for a given conference.
    'POST' - creates a new attendee for a given conference
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            attendees, encoder=AttendeesListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            content["conference"] = ConferenceVO.objects.get(
                import_href=conference_href
            )
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_attendee(request, id):
    """
    'GET' - returns the details of a specific attendee
    'DELETE' - deletes the specified attendee and returns a delete message
    'PUT' - updates the specified attendee and returns the updated attendee details
    """
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        if "conference" in content:
            try:
                content["conference"] = Conference.objects.get(
                    content["conference"]
                )
            except Conference.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid conference id"},
                    status=400,
                )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
