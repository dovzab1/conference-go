import requests
import json

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture_url(city, state):
    """gets a url from pexels for the city and state passed in
    and returns the url in a dict.
    """
    url = "https://api.pexels.com/v1/search"
    params = {"query": f"{city} {state}"}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=params)
    pexels_url = json.loads(response.content)
    if pexels_url["photos"]:
        picture_url = {
            "picture_url": pexels_url["photos"][0]["url"],
        }
    else:
        picture_url = {
            "picture_url": None,
        }
    return picture_url


def get_weather_data(city, state):
    """returns a dict with the weather for the city and state params"""
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},us",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    lat_lon = json.loads(response.content)
    if lat_lon:
        url = "https://api.openweathermap.org/data/2.5/weather"
        params = {
            "lat": lat_lon[0]["lat"],
            "lon": lat_lon[0]["lon"],
            "appid": OPEN_WEATHER_API_KEY,
            "units": "imperial",
        }
        response = requests.get(url, params=params)
        content = json.loads(response.content)
        weather = {
            "weather": {
                "temp": content["main"]["temp"],
                "description": content["weather"][0]["description"],
            }
        }
    else:
        weather = {"weather": "The weather is not available for this location"}
    return weather
