from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Conference, Location, State
from common.json import ModelEncoder
from .acls import get_picture_url, get_weather_data


class ConferenceListEncoder(ModelEncoder):
    """
    A class to encode a list of conferences.
    """

    model = Conference
    properties = ["name"]


class LocationListEncoder(ModelEncoder):
    """A class to encode a given Location."""

    model = Location
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    """
    A class to encode the details of a given Conference.
    """

    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

    def get_extra_data(self, o):
        return get_weather_data(o.location.city, o.location.state.abbreviation)


class LocationDetailEncoder(ModelEncoder):
    """A class to encode the details of a given location."""

    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference for 'GET'.
    Creates a new Conference for 'POST'.
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            content["location"] = Location.objects.get(id=content["location"])
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT", "GET"])
def api_show_conference(request, id):
    """
    Returns the details for the specified Conference using 'GET'.
    Deletes the specified conference using 'DELETE'.
    Updated the specified conference when using 'PUT'.
    """
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        if "location" in content:
            try:
                content["location"] = Location.objects.get(
                    id=content["location"]
                )
            except Location.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid location id"},
                    status=400,
                )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the locations when the method is GET.
    Creates a new location when the request method is POST.
    """
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            content["state"] = State.objects.get(abbreviation=content["state"])
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        picture_url = get_picture_url(content["city"], content["state"])
        content.update(picture_url)
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT", "GET"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified when using 'GET'.
    Deletes the specified location when using 'DELETE'.
    Updates the specified location when using 'PUT'.
    """
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        if "state" in content:
            try:
                content["state"] = State.objects.get(
                    abbreviation=content["state"]
                )
            except State.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid state abbreviation"},
                    status=400,
                )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
