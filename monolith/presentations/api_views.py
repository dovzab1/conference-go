from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Presentation
from common.json import ModelEncoder
from events.models import Conference
from events.api_views import ConferenceListEncoder


class PresentationListEncoder(ModelEncoder):
    """A class to encode a list of presentations."""

    model = Presentation
    properties = ["title"]


class PresentationDetailEncoder(ModelEncoder):
    """
    A class to encode the details of a Presentation
    """

    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    'GET' - returns a list of presentations for the specified conference
    'POST' - creates a new presentation and returns its data
    """
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            content["conference"] = Conference.objects.get(id=conference_id)
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):
    """
    'GET' - returns the details of the specified presentation
    'DELETE' - deletes the specified presentation and return a deleted msg
    'PUT' - updated the specified presentation and returns its details
    """
    if request.method == "GET":
        p = Presentation.objects.get(id=id)
        return JsonResponse(
            p,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        if "status" in content:
            if content["status"] == "approved":
                Presentation.objects.get(id=id).approve()
                del content["status"]
            elif content["status"] == "rejected":
                del content["status"]
                Presentation.objects.get(id=id).reject()
            else:
                return JsonResponse(
                    {"message": "Invalid status entry"},
                    status=400,
                )
        if "conference" in content:
            try:
                content["conference"] = Conference.objects.get(
                    id=content["conference"]
                )
            except Conference.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid conference id"},
                    status=400,
                )
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
